---
title: Library
subtitle: The specific things that I have found myself returning to most frequently
comments: false
---

## Library

It's been said: "If you want to know a man, look at his library."

- Zero to One by Peter Thiel
- 12 Rules for Life by Jordan Peterson
- 48 Laws of Power by Robert Green
- A Man for All Seasons by Robert Bolt
- Are We Good Enough for Liberty by Lawrence W. Reed
- Boundaries by Cloud & Townsend
- Economics in One Lesson by Henry Hazlitt
- Games People Play by Eric Berne M.D.
- Guide to Contentment by Fulton Sheen
- How Stuff Works by David Macaulay
- I, Pencil by Leonard E. Read
- Irresistible by Adam Alter
- Lost in the Cosmos by Walker Percy
- Made to Stick by Chip & Dan Heath
- No Man is an Island by Thomas Merton
- Originals by Adam Grant
- Outliers by Malcolm Gladwell
- Politics & Poetics by Aristotle
- Principles by Ray Dalio
- Start with Why by Simon Sinek
- Steve Jobs by Walter Isaacson
- The Bitcoin Standard by Saifedean Ammous
- The Design of Everyday Things by Donald Norman
- The Fifth Risk by Michael Lewis
- The Gulag Archipelago by Aleksandr Solzhenitsyn
- The New New Thing by Michael Lewis
- The Relationship Cure by John M. Gottman
- The Subtle Art of Not Giving A #@%! by Mark Manson
- The Undoing Project by Michael Lewis
- Thinking Fast and Slow by Daniel Khanemann
- Wherever You Go There You Are by Jon Kabat-Zinn
- Zen and the Art of Motorcycle Maintanence by Robert Pirsig
