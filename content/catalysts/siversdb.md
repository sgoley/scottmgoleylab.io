---
title: Businesses don't have a monopoly on databases
date: 2021-05-25
draft: false
tags: ["catalyst","entrepreneur","lessons","stories"]
---

<!--- TODO: Finish --->

https://sive.rs/dbt


Have a database for things.

As an example of where this helps you scale your personal productivity and outcomes, here's a start:

If you are job searching, use a personal CRM to track and manage your outbound leads (applications), send automated follow-ups after meetings, and more.

For instance, after a layoff - I used [Cloze](https://www.cloze.com/) in helping me track and automatically manage my interactions with more 200 job applications to companies through my email and more than 50 recruiters on LinkedIn.
