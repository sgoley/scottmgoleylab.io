---
title: Derek Sivers on FS
date: 2020-07-05
tags: ["podcasts", "podcastnotes"]
---

<!--- TODO: Finish --->

## Synopsis:

> Seth Godin is the author of 20 bestselling books, founder of altMBA, the Akimbo podcast and runs one of the most popular blogs in the world. Seth and Shane chat about creative work, fear, shame, trusting yourself, what it means to be a professional, how to become an observer of reality, emotional labor, how we learn and so much more.

## Topics:

* Intentions
* Leadership
* Authoring Books
* Amateurs vs Professionals
* Professionals follow a spec

## Surprising Thoughts:



## Embed:

<iframe width="560" height="315" src="https://www.youtube.com/embed/xIHx2H5YT60" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
