---
title: Therapy in a Nutshell - Why Coping can make it worse
date: 2021-06-27
draft: false
tags: ["therapy", "Explore"]
---

Analogous story:
There is a rare condition that some kids (100 in the world) may have where they are unable to feel pain at all.
On the surface it sounds good because they would never cry, they would never complain about pain. But as a consequence, they would allow themselves to become seriously hurt before noticing the damage.

------------------

Takeaways:

This applies to emotions equally. If you are unable to notice your emotional "pain" until it becomes seriously tramautic, you will have more serious consequnces to deal with.

The function of guilt is to ask: "did I do something wrong?"
The function of anxiety or fear is to ask: "is there something dangerous here?"
The function of hopelessness is to ask: "should I keep trying?"

Check against your values too.
Will adapting to those fears help you reach your goals or is it better to keep those fears intact as a warning mechanism?

False hopelessness / depression is to ask: "is this worth it, should I keep trying?" when there is no real cause to quit. That's why it's a disorder.

Emotions:
1) Warning Signs
2) Motivate Change
3) Connect to other people

Figure out where the emotions are coming from:
1) Address the emotional backlog
2) Be willing to feel a pure emotion
3) Explore secondary emotions

------------------

Exercises:

* Expore an intense emotion you've had recently.
* What purpose did it have? What kind of action was it asking for?
