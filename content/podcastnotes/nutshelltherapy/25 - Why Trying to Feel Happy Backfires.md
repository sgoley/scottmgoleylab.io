---
title: Therapy in a Nutshell - Why Trying to Feel Happy Backfires
date: 2021-10-19
draft: true
tags: ["therapy", "Explore"]
---

https://theoatmeal.com/comics/unhappy


------------------

Takeaways:

* Happiness is a result & outcome of living with purpose or doing meaningful things
* Happiness tends to evade people who seek it explicitly and directly whether through pleasure, satiation, or comfort

------------------

Exercises:

*
*
