---
title: Therapy in a Nutshell - How to stop struggling with emotions
date: 2021-07-01
draft: true
tags: ["therapy", "Explore"]
---

Intro about the cascading effects of feelings

When you get perceive negative emotion, by concentrating on feeling the negative emotion, you can amplify it.

There's a lot we can do to manage emotions indirectly through better management or processing etc. But the emotional responses that are taught while actually experiencing the emotion are relatively narrow:

* Relax
* Get over it
* Look on the bright side
* Push it down
Lots of people spend time supressing the emotions so they can get on with their lives.
Emotions consistently re-emerge until they have been processed and permanently resolved.


------------------

Takeaways:

What do you do when you try to avoid your feelings?
When the only tool that you have is to simply resist, you end up exhausted by the effort involved.

1. You judge emotions as good or bad.
2.

------------------

Exercises:

*
*
