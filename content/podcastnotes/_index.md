---
bookCollapseSection: true
weight: 10
---

<!--- TODO: Index --->

## A few of the favorites:

- [Farnham Street](/podcastnotes/fs)
- [Huberman Lab](/podcastnotes/huberlab)
- [Joe Rogan Experience](/podcastnotes/rogan)
- [The Portal with Eric Weinstein](/podcastnotes/theportal)
- [Naval](/podcastnotes/naval)
- [Rich Roll](/podcastnotes/richroll)
- [Tim Ferriss](/podcastnotes/timferriss)
- [What is Money](/podcastnotes/whatismoney)
