---
title: Bret Easton Ellis on The Portal w/ Eric Weinstein
date: 2020-07-05
tags: ["podcasts", "podcastnotes"]
---

## Synopsis:

> Eric sits down with Bret Easton Ellis; the two Gen X’ers graduated from rival high schools in a disaffected 1982 Los Angeles that inspired Ellis’ first novel “Less Than Zero”.

## Topics:


## Surprising Thoughts:



## Embed:

<iframe src="https://www.art19.com/shows/the-portal/episodes/b51bf85d-b4b0-4920-a3c6-d08d8291c178/embed" style="width: 100%; height: 200px; border: 0 none;" scrolling="no”></iframe>
