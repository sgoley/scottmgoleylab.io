---
title: Comment on managing risk for options
date: 2020-06-03
draft: true
tags: ["options","strategy"]
---

> - High volatility and I think it's over valued: sell puts at the price I think it's worth.
> -  High volatility and I think it's undervalued: buy shares and sell covered calls.
> - Low volatility and I think it's over valued: buy some fractions and wait to see if I still want to get in after some time. I find after owning some of a stock, even just a little, I lose interest in it unless it's something I really believe in.
> - Low volatility and I think it's under valued: buy some long dated calls.