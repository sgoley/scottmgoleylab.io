---
title: Liquidity Mining with Hummingbot
date: 2021-02-10
draft: true
tags: ["crypto", "defi","altcoins"]
---

Starting a liquidity miner with hummingbot using the docker version on windows.

Here we go.

1. Start your exchange setup / verification process.
This can take some time, so I'd recommend doing this earliest.

2. - just do this first for simplicity, upgrade to WSL2 FIRST
https://docs.microsoft.com/en-us/windows/wsl/install-win10

3. - going to assume that you have docker desktop installed:
https://www.docker.com/products/docker-desktop

4. - setup docker WSL2 backend:
https://docs.docker.com/docker-for-windows/wsl/

5. - docker pull the hummingbot image:
https://hub.docker.com/r/coinalpha/hummingbot

`docker pull coinalpha/hummingbot`

6. - setup your password / etc for your bot

7. -
