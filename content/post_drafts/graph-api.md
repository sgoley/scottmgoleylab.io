---
title: Custom Connector in a day
date: 2018-11-15
draft: true
tags: ["graphapi", "apis","customconnector"]
---

# Table of Contents
1. [Graph API](#graph-api)
2. [What is Graph good for?](#What-is-Graph-good-for?)
3. [Custom Connector](#custom-connector)

## Graph API

The Graph API is provided by Azure to provide insights on Azure Directory users and their activities.


[Graph API](https://graph.microsoft.com)


Docs for reference.

## What is Graph good for?

User Logins
User


## Custom Connector

In order to connect to your Graph information - logins and user activity in your active directory domain -
unfortunately, a generic adapter is not yet available but this is a walk through in building your own custom connector.
