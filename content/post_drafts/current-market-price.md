---
title: Current Market Price
date: 2020-05-01
draft: true
tags: ["markets","trading","finance"]
---

A point that I'd like to make to deconstruct something within financial markets reporting is the idea of "representative" absolute price.

For instance, in this article about [Costco Q4 2020 earnings](https://www.barrons.com/articles/costcos-latest-strong-quarter-still-wasnt-good-enough-51600982782),
the following appears:

> Costco was down 2.9% to $337 around 6:38 a.m. Eastern time in premarket trading. That might seem odd, given how well the company outperformed key metrics, but the shares did much the same last quarter.
