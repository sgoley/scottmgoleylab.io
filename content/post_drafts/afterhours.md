---
title: Afterhours Trading
date: 2020-05-01
draft: true
tags: ["markets","trading","finance"]
---

During my renewed interest in trading this year, one of the things that has most deeply attracted my attention is the exploration of the impact of afterhours and premarket trading on the momentum for the rest of the day.

Quite frequently this is where big moves can happen but very typically at a fraction of the same instrument's daily trading volume.
Looking into AH big ask spreads, it is not uncommon to see the limit buy (the bid) for an instrument with no active news to collapse to a penny or simply a fraction of the bid during the normal market hours. 
