---
title: Intro to Crypto
date: 2021-05-01
draft: false
tags: ["cryptocurrencies", "bitcoin", "ethereum"]
---

-----------------  -----------------
#### Resources I've used for understanding cryptocurrencies

Motivation and philosophy:
https://www.youtube.com/watch?v=mDyBbGCiBUU

What is money and why do we need a decentralized "money":
https://www.youtube.com/c/RobertBreedlove22

The inner workings:
https://www.youtube.com/watch?v=3x1b_S6Qp2Q

The case for bitcoin (specifically):
https://casebitcoin.com/newbie

The legendary rainbow log price chart:
https://www.blockchaincenter.net/bitcoin-rainbow-chart/

Bitcoin vs Altcoins (Everything other than Bitcoin):
https://www.blockchaincenter.net/altcoin-season-index/

  ---------------------------------  ----------------------------------
#### Security

You will effectively now be your own BANK. No one can help you if you do something wrong.

* Keep your passwords secure. Keep your backups and restore phrases secure.
* Only use your wallets as necessary.
* *** Have a "hot" wallet with minimal funds and a "cold" wallet for long term savings.
* Setup multi-factor on everything (SMS + Email code + really good password)
* Double check addresses on every single transaction.
* SEND TEST TRANSACTIONS of small amounts first (Ex. 0.001 ETH before you send X.0 ETH).
* If you use metamask do not move funds to it UNTIL you set the [AUTO-LOCK feature](https://coinflex.com/blog/metamask-security-how-to-secure-your-metamask-account/). 
	*The biggest source of Metamask stolen funds is browsing with an unlocked wallet.*

More really useful and relevant articles: https://www.gemini.com/cryptopedia/explore#security

  ------------------------------------------------------------------
#### US Centralized Exchanges

Coinbase
http://coinbase.com/
Common but historically lots of reliability issues (Website down, App not working, long support times)

Kraken
https://www.kraken.com/
Not as common as Coinbase, but built for crypto "professionals". Lots of investment into trading platform etc. Decent Controls, Decent Support.

Gemini
https://gemini.com/
Even less commonly used than Kraken, but built to be the most "trusted" option. Lots of KYC, "Financial Compliance" but generally best for ease of use & reliability.

NON-EXCHANGES:
Paypal / Square / Robinhood

These will not let you actually transfer crypto to a private wallet.
You are simply buying exposure to crypto but it will be owned by someone else and your control / access will be governed by someone else.

  -------------------------------------------------------------------
#### Wallets

Desktop Wallets
(If you are comfortable with YOUR computer as your bank - treat it like your bank - don't take out of the house, don't connect to public wifi, use a vpn, don't torrent ANYTHING, etc. )
Exodus - https://www.exodus.com/
Coinomi - https://www.coinomi.com/en/
Atomic Wallet - https://atomicwallet.io/

Hardware Wallets:
Trezor - https://trezor.io/
Ledger - https://www.ledger.com/

Paper Wallets:
https://www.gemini.com/cryptopedia/paper-wallet-generator-cold-storage#section-how-to-make-a-paper-wallet

-----------
#### Disclaimer

Above all, treat everything skeptically. Don't take my words, do your own research. I'm not your financial advisor - just a person on the internet. 