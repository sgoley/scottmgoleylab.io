---
title: The one with the cloud ETL
date: 2018-06-01
draft: true
tags: ["integromat", "apis"]
---

# Table of Contents
1. [Integromat Basics](#basics)
2. [HTTP Calls](#http-calls)
3. [Google APIs](#google-apis)
4. [Scenarios](#scenarios)
5. [Integromat Webhooks](#integromat-webhooks)
6. [Google Fusion Tables](#google-fusion-tables)



## Integromat Basics

- What is Integromat?

It is a cloud process automation platform that utilizies integrations to perform operations via the simplest means possible. No ETL layer, no instance management, no cron jobs - just an approachable

- Why not just use Zapier?

My main issue with zapier is that you can only integrat with the services that they have built native connectors for and in addition to that you are inherently somewhat limited on the operation chains that you can generate.

- Who is it for?

If IFTTT is for your average consumer and Zapier for your standard office user, and you aren't working

In the interest of breaking that chain, they introduced a fully customizable HTTP operator that can make scheduled or operation based callouts in an extremely approachable way. At least from what I've found personally, it's been the best option for working with a number of custom operation chains that would require multiple zaps, a webserver and scheduler, or a number of other solutions that are not as approachable.

## HTTP Calls


## Google APIs

Now using HTTP callouts with google APIs are a bit more particular than your standard api resource.

select * from 1YhqdMZaAsw3xN1oNaSvxX6VyMW16-cwHolwJEpuK WHERE type_facility {{switch(6.treatment_type; "Substance Abuse"; "= 'SA'"; "Mental Health"; "= 'MH'"; "Both"; "IN ('MH', 'SA')")}} AND LicenseAndAccredited > 0
ORDER BY ST_DISTANCE(latitude, LATLNG({{6.latitude}}, {{6.longitude}})) LIMIT 3


## Scenarios

## Integromat Webhooks

## Google Fusion Tables
