---
title: Crypto Platforms & Trading Tools
draft: false
tags: ["crypto","tradingtools"]
---

* Screening
 - [Sanbase](https://app.santiment.net/)
 - [CoinMarketCap](https://coinmarketcap.com/)
 - [Cryptowat.ch](https://cryptowat.ch/assets)
 - [Wallmine](https://wallmine.com/market/us)
 - [Messari](https://messari.io/)
* Options
 - [Deribit](https://www.deribit.com/)
 - [LedgerX / FTX US](https://derivs.ftx.us/)
* Charting
 - [Cryptowat.ch](https://cryptowat.ch/)
 - [TradingView](https://www.tradingview.com/)
* Liquidity
 - [TensorCharts](https://www.tensorcharts.com/)
 - [BookMap](https://bookmap.com/)
* Alternative Information
 - [CryptoQuant](https://cryptoquant.com/overview/btc-exchange-flows)
 - [CoinGlass / ByBt](https://www.coinglass.com/)
 - [GlassNode](https://glassnode.com/)
 - [Rainbow Chart](https://www.blockchaincenter.net/bitcoin-rainbow-chart/)
 - [Laevitas](https://app.laevitas.ch/dashboard)
* DeFi
 - [Gas Calculator](https://defiyield.info/gas-cost-tracker)
 - [Impermanent Loss Calculator](https://defiyield.app/advanced-impermanent-loss-calculator)
 - [Zapper Dashboard](https://zapper.fi/)
 - [InstaDAPP Dashboard](https://defi.instadapp.io/)
